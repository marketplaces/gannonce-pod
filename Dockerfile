FROM node:16.16
WORKDIR /app
COPY . .
RUN rm -rf /app/node_modules

ENV NODE_ENV=production
RUN npm install

CMD ["node", "bin/gannonce", "gannonce", "0.0.0.0"]
EXPOSE 8600